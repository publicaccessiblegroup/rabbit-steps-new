// We can also use destructuring of array to split the string characters
const splittedString = [..."Hello World"];
console.log(`The splitted string: `);
console.log(splittedString);
