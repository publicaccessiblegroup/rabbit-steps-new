// We can use array destructuring for swapping variables as shown,
let [num__1, num__2] = [1, 2];
console.log(`Before swapping: ${num__1}, ${num__2}`);
[num__2, num__1] = [num__1, num__2];
console.log(`After swapping: ${num__1}, ${num__2}`);
