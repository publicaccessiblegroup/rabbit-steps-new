// Rest and Spread operators are binded with Destructuring in ES6
const [str_1, str_2, ...arr] = ["Hello", "World", 1, 2, 3, 1.1123]; // We are resting the elements inside variable - arr
const arr1 = [...arr]; // "..." used to spread the elements
// "..." is considered as both rest and spread operator depending on the context given below
// If the "..." appears on the left hand side then it is called "Rest Operator"
// If-Else the "..." appearing on the right hand side then it is called "Spread Operator"
