/**
 * Arrays are indexed from 0 and Array.length - 1, in-order to access the elements
 * of the array we make use indexing concept applied on the Array
 *
 * Eg:
 * const sampleArray = ["Hello", "World", 1, 2, 3, 1.1123];
 * 1st element -> sampleArray[0];
 * 2nd element -> sampleArray[1];
 * 3rd element -> sampleArray[2];
 * ...
 *
 * Assigning the elements to variables,
 *
 * const var_1 = sampleArray[0];
 * const var_2 = sampleArray[1];
 * const var_3 = sampleArray[2];
 * ...
 *
 * This is not bad practice, but is it possible to assign them in one single line
 * Yes, This is possible by using the destructure concept introduced ES6
 */
