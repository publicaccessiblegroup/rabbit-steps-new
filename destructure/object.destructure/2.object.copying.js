// What if we want to copy one object ?
// If we use assignment operator to copy the object then we will be doing copy by reference instead of copy by value...
// In this case if we modify the copied object, then the changes are reflected in the original object as well
const otherObj = obj;
console.log("The copied object: ");
console.log(otherObj);
otherObj["name"] = "maisie"; // changing the value of name in "otherObj" but not in "obj"
console.log("The changes are reflected in the original object as well: ");
console.log(obj); // but this one we always need to avoid

// In order to solve the above problem we make use of "Spread" operator

const obj_1 = {
    name: "Jordan",
    movie: "The Sea Beast",
    released: "2022",
};
const otherObj_1 = {
    ...obj_1, // using spread operator
};
otherObj_1["name"] = "maisie";
console.log("The changes are not reflected in the original object: ");
console.log(otherObj_1);
console.log(obj_1); // The original object is not effected by the change in copied object
