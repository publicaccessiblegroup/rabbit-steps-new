const obj = {
    name: "Jordan",
    movie: "The Sea Beast",
    released: "2022",
};

const { name, movie, released } = obj;
console.log(`Name of character ${name} in the movie ${movie} released on ${released}`);
