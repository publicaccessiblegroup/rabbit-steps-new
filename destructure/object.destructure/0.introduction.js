/**
 * Till now we saw the Array destructuring, What about object destructing is it possible ?
 * 
 * const obj = {
        name: "Jordan",
        movie: "The Sea Beast",
        released: "2022"
    }
 * Traditional way of assigning objects matches the Array initializing
    const name = obj["name"];
    const movie = obj["movie"];
    const released = obj["2022"];
 
 * This is fine but tedious process to do ... So we use the same destructure concept
 */
