// We can also concatenate the two objects together
const object_2 = {
    movie: "Jurassic Park",
};

const object_3 = {
    dinosaur: "Velociraptor",
};

const concatenatedObject = {
    ...object_2,
    ...object_3,
};
console.log(concatenatedObject); // concatenated object of object_2 and object_3
