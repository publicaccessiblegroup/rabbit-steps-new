// We can also use the object destructing in function parameter / method parameter
const movieFunction = ({ name, movie, released }) => {
    // the order we write the variables in the parameter is not considered
    // it can be of any format -> { movie, name, released } as well
    console.log(`Name of character ${name} in the movie ${movie} released on ${released}`);
};

movieFunction(obj);
