const array = ["Hello", "World", "...", "How", "are", "you", "?"];

// The task for us is to iterate through the array and construct the sentence out of it
// Output -> "Hello World... How are you ?"

// We can simply loop over it using the traditional looping construct
const traditionalLoop = () => {
    let wholeString = "";
    for (let index = 0; index < array.length; index++) {
        wholeString += array[index] + " ";
    }
    return wholeString;
};

const completeString = traditionalLoop();
console.log(completeString); // Output -> Hello World ... How are you ?
