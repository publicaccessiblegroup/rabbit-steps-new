/**
 * Loops play a vital roll in doing repetitive tasks
 * There are three types loops in JavaScript
 *  1. for loop
 *  2. while loop
 *  3. forEach loop
 *  4. do-while loop

 * In the following snippet, we will look at the constructs "of" and "in" that can be used along with loop...

 */
