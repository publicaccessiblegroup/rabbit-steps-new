const array = ["Hello", "World", "...", "How", "are", "you", "?"];

// Instead of using traditional of using for loop where we initialize, check the condition and increment approach
// we can iterate the iterable objects using "of" construct provided by JavaScript

const forOfLoop = () => {
    let wholeString = "";
    for (let element of array) {
        wholeString += element + " ";
    }
    return wholeString;
};

const completeString_ = forOfLoop();
console.log(completeString_); // Output -> Hello World ... How are you ?
