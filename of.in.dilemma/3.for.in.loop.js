// Let us first consider,
const array = ["Hello", "World", "...", "How", "are", "you", "?"];

// Now what if we use "in" construct in the for loop
const forInLoop = () => {
    let wholeString = "";
    for (let element in array) {
        wholeString += element + " ";
    }
    return wholeString;
};

const completeString_1 = forInLoop();
console.log(completeString_1);

// Here we do expect -> Hello World ... How are you ? but actual -> 0 1 2 3 4 5 6
